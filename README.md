# MIDI-MT

#### MIDI-MT is High Level Application Driver for USB MIDI Control Surface

# MIDI-MT features

- Mackie translator for Adobe production: `Premiere Pro`, `After Effects`, `Audition` etc.
- Using MIDI-MT as a `MIDI proxy` server, or `HUB` server mode.
- Mixer and Audio panel of audio sessions of running applications.
- Smart House controls: `MQTT`, `DMX512`, `ARTNET`, `BOBLIGHT`.
- Interactive remote `Web Control panel`.
- User `macro/scripting` available: Chaiscript (ECMA 6 syntax compatible).
- Supporting any `USB/MIDI` Control Surfaces as __WORDLE__, __Pyle Audio PMIDIPD30__, __LAudio__, __Fegoo__ manufacture and other. 
- Supporting `Joystick` or/and `Gamepad` as a control panel, or such as remote control.  
- Include `Control Panel Designer` - to design your own control panels to manage the controls configured in the configuration file.

Moved to: [MIDI-MT](https://bitbucket.org/midi-mt/midi-mt-src/src/main/releases/)
